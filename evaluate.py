import pandas as pd



# Чтение данных
winners = pd.read_json('data/winners.json')
answers = pd.read_json('data/answers.json')
companies = pd.read_json('data/companies.json')
ext_offers = pd.read_json('data/ext_offers.json')
items = pd.read_json('data/items.json')
offer_items = pd.read_json('data/offer_items.json')
offers = pd.read_json('data/offers.json')
questions = pd.read_json('data/questions.json')
stages = pd.read_json('data/stages.json')
tenders = pd.read_json('data/tenders.json')

# Известные компании
companies_part = companies.loc[:, ['id','title_full','inn']]
companies_part = companies_part.rename(columns={'id':'party_id','title_full':'title'})
companies_part['party_id'] = companies_part['party_id'].apply(str)

# Внешние компании
ext_offers_part = ext_offers.loc[:,['party_id','title','inn']].drop_duplicates()

# Объединяем для удобства
all_companies = pd.concat([companies_part,ext_offers_part])

row = items['winnerid']
# Забираем из items тех, что являются победителями
result_base = items.loc[row!=''].loc[:,['id','number','tender_id','name','winnerid']]
# Объединяем с информацией о компаниях
result_base = pd.merge(result_base,all_companies,left_on='winnerid',right_on='party_id')
# формируем "Победителя"
result_base['title'] = result_base['title']+result_base['inn'].apply(lambda x: (' ИНН: ' + str(x)) if x != None and str(x) != '' else ' Без ИНН')
winners_id_series = result_base['party_id']
# Формируем каркас итоговой таблицы
result_table = result_base.loc[:,['number','name','title']].rename(columns={'number':'#','name':'Позиция','title':'Победитель'})

# stages_count = 0
# for stage in stages.iterrows():
#     col_name = f"Этап {stage[1]}"
#     stage_id = stage[0]
#     stages_count += 1
    

# For test
# print(all_companies)
# print(result_base)
# print(result_base.loc[:,['number','name','title']].rename(columns={'number':'#','name':'Позиция','title':'Победитель'}))


# Записываем отчет
writer = pd.ExcelWriter("report.xlsx",engine='xlsxwriter')
result_table.to_excel(writer,sheet_name='История ставок победителя', index=False)
writer.save()

print("Done")